//
//  ViewController.m
//  ViewsBreakout
//
//  Created by James Cash on 07-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIView *redView = [[UIView alloc] initWithFrame:CGRectZero];
    redView.backgroundColor = UIColor.redColor;
    redView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:redView];
    [NSLayoutConstraint activateConstraints:
     @[[redView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:20],
       [redView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-20]]];

    UIView *orangeView1 = [[UIView alloc] initWithFrame:CGRectZero];
    orangeView1.backgroundColor = UIColor.orangeColor;
    orangeView1.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:orangeView1];

    UIView *orangeView2 = [[UIView alloc] initWithFrame:CGRectZero];
    orangeView2.backgroundColor = UIColor.orangeColor;
    orangeView2.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:orangeView2];

    [NSLayoutConstraint activateConstraints:
     @[[orangeView1.widthAnchor constraintEqualToConstant:50],
       [orangeView2.widthAnchor constraintEqualToConstant:80],
       [orangeView1.heightAnchor constraintEqualToConstant:50],
       [orangeView2.heightAnchor constraintEqualToAnchor:orangeView1.heightAnchor],
       [orangeView1.centerYAnchor constraintEqualToAnchor:redView.centerYAnchor],
       [orangeView2.centerYAnchor constraintEqualToAnchor:orangeView1.centerYAnchor],

       [orangeView1.leadingAnchor constraintEqualToAnchor:redView.leadingAnchor constant:8],
       [orangeView1.trailingAnchor constraintEqualToAnchor:orangeView2.leadingAnchor constant:-8],
       [orangeView2.trailingAnchor constraintEqualToAnchor:redView.trailingAnchor constant:-8],

       [orangeView1.topAnchor constraintEqualToAnchor:redView.topAnchor constant:8]

       ]];

    UIView *blueView = [[UIView alloc] initWithFrame:CGRectMake(100, 20, 300, 200)];
    blueView.backgroundColor = UIColor.blueColor;
//    [self.view addSubview:blueView];
    [self.view insertSubview:blueView belowSubview:redView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
